<?php
session_start();

if(isset($_SESSION['user_id']) and isset($_SESSION['administrator']) and $_SESSION['administrator'] == 'SI') {

//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

$conn = new mysqli($servername, $username, $password, $database);

$email= $_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->

<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<script type="text/javascript" src="./js/jquery.dNotify.min.js"></script>
<link rel="stylesheet" type="text/css" href="./css/dNotify.css" media="screen" />
<script type="text/javascript" src="./js/jquery.dNotify.js"></script>

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>
<?php 
//da finire
	if (isset($_GET['result']) and isset($_GET['type'])) {
		if (strtolower($_GET['type']) == 'update') {
			if (strtolower($_GET['result']) == 'true') {
				echo "<script>
					$.dNotify(\"show\", {
   						message: 'Prodotto aggiornato correttamente',
    					sticky: false,
   						duration: 5000,
  						type: 'good',
						iconPosition: 'right'
					});</script>";
			} else {
				echo "<script>
					$.dNotify(\"show\", {
   						message: 'Errore nell'aggiornamento del prodotto',
    					sticky: false,
   						duration: 5000,
  						type: 'bad',
						iconPosition: 'right'
					});</script>";
			}
		} else {
			if (strtolower($_GET['result']) == 'true') {
				echo "<script>
					$.dNotify(\"show\", {
   						message: 'Prodotto eliminato correttamente',
    					sticky: false,
   						duration: 5000,
  						type: 'good',
						iconPosition: 'right'
					});</script>";
			} else {
				echo "<script>
					$.dNotify(\"show\", {
   						message: 'Errore nell'eliminazione del prodotto',
    					sticky: false,
   						duration: 5000,
  						type: 'bad',
						iconPosition: 'right'
					});</script>";
			}
		}

}
?>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="addNewFood.php"><span>Aggiungi prodotto</span></a></li>
										<li><a href="administratorOrderList.php"><span>Tutti gli ordini</span></a></li>
										<li><a href="index.php"><span><i class="iconColor fa fa-sign-out fa-2x" aria-hidden="true"></i></span></a></li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<!-- welcome -->
	<div class="about">
		<div class="container">
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Piadine
					</h2>
					<br/>
				</div>
				<?php
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else {
						$query_sql="SELECT * FROM piadine";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center"></th><th align="center">Nome</th><th align="center">Ingredienti</th><th align="center">Prezzo</th><th></th>
						</theader>
						<tbody>
							<?php
								while($row = $result->fetch_assoc()) {
							?>
								<form method="post" action="updateProducts.php">
									<tr>
										<td>
											<a href="deleteProducts.php?id=<?php echo $row['id']; ?>&type=piadine"><span><i class="fa fa-minus" style="color:red;" aria-hidden="true"></i></span></a>
										</td>
										<td>
											<input name="name" id="name" type="text" value="<?php echo $row['nome']; ?>"/>
											<input name="type" id="type" type="hidden" value="piadine"/>
											<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
										</td>
										<td>
											<input name="ingredients" id="ingrediets" type="text" value="<?php echo $row['ingredienti']; ?>"/>
										</td>
										<td>
											<input name="price" id="price" type="text" value="<?php echo $row['prezzo']; ?>"/>
										</td>
										<td> 
											<input class="submit" type="submit" value="Salva" />
										</td>
									</tr>
								</form>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							}
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Crescioni
					</h2>
					<br/>
				</div>
				<?php
					$conn = new mysqli($servername, $username, $password, $database);
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else{
						$query_sql="SELECT * FROM crescioni";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center"></th><th align="center">Nome</th><th align="center">Ingredienti</th><th align="center">Prezzo</th><th></th>
						</theader>
						<tbody>
							<?php
								
								while($row = $result->fetch_assoc()) {
							?>
							<form method="post" action="updateProducts.php">
									<tr>
										<td>
											<a href="deleteProducts.php?id=<?php echo $row['id']; ?>&type=crescioni"><span><i class="fa fa-minus" style="color:red;" aria-hidden="true"></i></span></a>
										</td>
										<td>
											<input name="name" id="name" type="text" value="<?php echo $row['nome']; ?>"/>
											<input name="type" id="type" type="hidden" value="crescioni"/>
											<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
										</td>
										<td>
											<input name="ingredients" id="ingrediets" type="text" value="<?php echo $row['ingredienti']; ?>"/>
										</td>
										<td>
											<input name="price" id="price" type="text" value="<?php echo $row['prezzo']; ?>"/>
										</td>
										<td> 
											<input class="submit" type="submit" value="Salva" />
										</td>
									</tr>
								</form>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							} else {
								echo "Non ci sono ordini che non sono arrivati al destinatario per sue cause";
							}
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Rotoli
					</h2>
					<br/>
				</div>
				<?php
					$conn = new mysqli($servername, $username, $password, $database);
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else {
						$query_sql="SELECT * FROM rotoli";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center"></th><th align="center">Nome</th><th align="center">Ingredienti</th><th align="center">Prezzo</th><th></th>
						</theader>
						<tbody>
							<?php
								
								while($row = $result->fetch_assoc()) {
							?>
							<form method="post" action="updateProducts.php">
									<tr>
										<td>
											<a href="deleteProducts.php?id=<?php echo $row['id']; ?>&type=rotoli"><span><i class="fa fa-minus" style="color:red;" aria-hidden="true"></i></span></a>
										</td>
										<td>
											<input name="name" id="name" type="text" value="<?php echo $row['nome']; ?>"/>
											<input name="type" id="type" type="hidden" value="rotoli"/>
											<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
										</td>
										<td>
											<input name="ingredients" id="ingrediets" type="text" value="<?php echo $row['ingredienti']; ?>"/>
										</td>
										<td>
											<input title="usare il punto ( . ) invece della virgola ( , )" name="price" id="price" type="text" value="<?php echo $row['prezzo']; ?>"/>
										</td>
										<td> 
											<input class="submit" type="submit" value="Salva" />
										</td>
									</tr>
								</form>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							} else {
								echo "Non sono stati consegnati ordini";
							}
						}
						else {
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
		</div>
	</div>
<!-- //welcome -->

<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->

<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
<?php } ?>
