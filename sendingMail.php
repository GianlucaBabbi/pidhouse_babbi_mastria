<?php
include("./PHPMailer-master/src/PHPMailer.php");
include("./PHPMailer-master/src/Exception.php");
include("./PHPMailer-master/src/OAuth.php");
include("./PHPMailer-master/src/POP3.php");
include("./PHPMailer-master/src/SMTP.php");
include("./PHPMailer-master/src/class.phpmailer.php");

function inviomail($email, $oggetto, $messaggio, $path_allegato){

    if(filter_var($email, FILTER_VALIDATE_EMAIL)){ // validate_email � una funzione che controlla la correttezza della mail
        //invia la mail
        $mail = new PHPMailer();
        $mail->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
        $mail->SMTPAuth = true; // Autenticazione SMTP
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->SMTPSecure = 'ssl';
        $mail->Username = "pidhouseCesena@gmail.com"; // Nome utente SMTP autenticato
        $mail->Password = "pidhouse"; // Password account email con SMTP autenticato
        $mail->Priority    = 1; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
        $mail->From     = "servercustom.noreply@gmail.com";
        $mail->FromName = "PidHouse Cesena";
        $mail->AddAddress($email);
        $mail->IsHTML(true);
        $mail->Subject  =  $oggetto;
        $mail->Body     =  $messaggio;
        $mail->AltBody  =  "";
        $mail->AddAttachment($path_allegato); 
        if(!$mail->Send()){
                header("location: orders.php?result=false");
        } else {
			if (isset($_GET['destinatario'])) {
				header("location: administratorOrderList.php");
			} else {
				header("location: orders.php?result=true");
			}
        }
    }else{
        echo "La mail non � stata inviata a causa dell'indirizzo errato o un problema di connessione!";
    }
}

session_start();
$email = '';
$messaggio = '';
if (isset($_GET['destinatario'])) {
	if (strtolower($_GET['statoConsegna']) == 'consegnato') {
		$messaggio = "Il suo ordine e' stato consegnato";
	} else if (strtolower($_GET['statoConsegna']) == 'destinatario_non_trovato') {
		$messaggio = "Il suo ordine al momento della consegna ha trovato il destinatario assente";
	} 
	
	$email = $_GET['destinatario'];
} else {
	$email = $_SESSION['email'];
	$messaggio = "L'ordine appena effettuato � in fase di preparazione e verr� consegnato il prima possibile con un limite massimo di 30 minuti, la preghiamo di attendere all'indirizzo di consegna da lei inserito.";
}

$oggetto = 'Conferma ordine';

$messaggioPidhouse = $_SESSION['email']." ha appena effettuato un nuovo ordine: " .$_SESSION['prodottiAcquistati']." da portare in " .$_SESSION['shippingAddressess']." a ".$_SESSION['city'];
//controllare messaggio sopra e aggiungere quantit�
if ($messaggio != '') {
	inviomail($email, $oggetto, $messaggio, '');
	inviomail('pidhousecesena@gmail.com', 'Nuovo ordine da preparare', $messaggioPidhouse, '');
} else {
	header("location: administratorOrderList.php");
}
?>