<?php

session_start();

if(isset($_SESSION['user_id'])) {
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

$conn = new mysqli($servername, $username, $password, $database);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->

<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<script type="text/javascript" src="./js/jquery.dNotify.min.js"></script>
<link rel="stylesheet" type="text/css" href="./css/dNotify.css" media="screen" />
<script type="text/javascript" src="./js/jquery.dNotify.js"></script>

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>
<?php 
	if (isset($_GET['result'])) {
		echo "<script>$.dNotify('show', {
   			message: 'La tua carta di credito risulta scaduta',
			sticky: false,
 			duration: 2000,
  			type: 'bad',
			iconPosition: 'right'
			});</script>";
}
?>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="list.php?listino=piadine"><span>Piadine</span></a></li>
										<li><a href="list.php?listino=rotoli"><span>Rotoli</span></a></li>
										<li><a href="list.php?listino=crescioni"><span>Crescioni</span></a></li>
										<li><a href="cart.php"><span><i class="iconColor fa fa-shopping-cart fa-2x" aria-hidden="true"></i></span></a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-hover="Short Codes"><i class="iconColor fa fa-user fa-2x" aria-hidden="true"></i></span> <b class="caret"></b></a>
											<ul class="dropdown-menu agile_short_dropdown">
												<li><a href="orders.php">I miei ordini</a></li>
												<li><a href="personalData.php">I miei dati</a></li>
												<li><a href="index.php">Log out</a></li>
											</ul>
										</li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<div class="about">
		<div class="container">
			<div class="panel-body" style="width:50% !important; margin: 0 25%;">
				<div class="w3ls-heading">
					<h2>Dati Pagamento</h2>
					<br/>
				</div>
							<form method="post" action="creditCardCheck.php">
								<div align="center" >
									<table class="table-responsive">
										<tbody>
											<tr >
												<td colspan="2">
													<label for="owner">Titolare carta di credito</label>
													<input style="text-align: center; width:100%;" name="owner" width="100%" type="text" maxlength="16" required>
												</td>
											</tr>
											<tr >
												<td colspan="2">
													<label for="cartNumber">Numero carta di credito</label>
													<input style="text-align: center; width:100%;" name="cartNumber" width="100%" type="text" maxlength="16" required>
												</td>
											</tr>
											<tr>
												<td align="left">
													<label for="month">Mese di scadenza</label>
													<?php
														$mesi = array("gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre");
														$annoDiPartenza = date("Y",time());
														$annoFinale =  $annoDiPartenza + 30;
													?>
													<select style="text-align: center; width:90%;" name="month" id="month">
													<?php
														foreach($mesi as $numero => $mese) {
															echo '<option value="'.($numero+1).'">'.$mese.'</option>';
														}	
													?>
													</select>
												</td>
												<td align="right">
													<label for="year">Anno di scadenza</label>
													<select style="text-align: center; width:90%;" name="year" id="year">
														<?php 
														echo '';	
														for($i = $annoDiPartenza; $i<= $annoFinale; $i++){
															echo '<option value="'.$i.'">'.$i.'</option>';
														}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td  colspan="2">
													<label for="cvv">CVV</label> 
													<input style="text-align: center; width:100%;" name="cvv" width="100%" type="text" maxlength="3" required>
												</td>
											</tr>
										</tbody>
									</table>
									<!-- inserire immagini -->
								</div>
								<br/>
								<div align="center">
									<input class="submit" style="text-align: center; width:60%;" type="submit" value="Paga"/>
								</div>
								<input type="hidden" value="Paga" id="salva" name="salva" />
							</form>
			</div>
		</div>
	</div>
<!-- //welcome -->

<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->

<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
<?php } ?>
