<?php

session_start();

if(isset($_SESSION['user_id'])) {
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

$conn =new mysqli($servername, $username, $password, $database);

$_SESSION['totalPrice']= $_POST['totPrice'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="list.php?listino=piadine"><span>Piadine</span></a></li>
										<li><a href="list.php?listino=rotoli"><span>Rotoli</span></a></li>
										<li><a href="list.php?listino=crescioni"><span>Crescioni</span></a></li>
										<li><a href="cart.php"><span><i class="iconColor fa fa-shopping-cart fa-2x" aria-hidden="true"></i></span></a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-hover="Short Codes"><i class="iconColor fa fa-user fa-2x" aria-hidden="true"></i></span> <b class="caret"></b></a>
											<ul class="dropdown-menu agile_short_dropdown">
												<li><a href="#">I miei ordini</a></li>
												<li><a href="personalData.php">I miei dati</a></li>
												<li><a href="index.php">Log out</a></li>
											</ul>
										</li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<div class="about">
		<div class="container">
			<div class="panel-body" style="width:50% !important; margin: 0 25%;">
				<div class="w3ls-heading">
					<h2>Dati di Consegna</h2>
					<br/>
				</div>
				<?php
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else{
						
						$query_sql="SELECT * FROM utenti_registrati where email='" .$_SESSION['email'] ."'";
						$result = $conn->query($query_sql);
						if($result !== false){
				?>
				
							<?php
								if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {
							?>
							<form method="post" action="sessionVariables.php">
								<div >
									<label for="name">Nome</label><br/>
									<?php echo "<input style=\"width:100%;\" name=\"name\" width=\"100%\" type=\"text\" pattern=\".{1,30}\" required title=\"Lunghezza minima 1 carattere\" value=\"" .$_SESSION['user_id'] ."\">"; ?>
									<br/>
									<label for="surname">Cognome</label><br/> 
									<?php echo "<input style=\"width:100%;\" name=\"surname\" width=\"100%\" type=\"text\" pattern=\".{1,30}\" required title=\"Lunghezza minima 1 carattere\" value=\"" .$_SESSION['user_surname'] ."\">"; ?>
									<br/>
									<label for="surname">Mail</label><br/>
									<?php echo "<input style=\"width:100%;\" name=\"mail\" width=\"100%\" type=\"text\" disabled value=\"" .$_SESSION['email'] ."\">"; ?>
									<br/>
									<label for="adress">Indirizzo di consegna</label><br/>
									<input pattern=".{1,50}" required title="Lunghezza minima 1 carattere" style="width:100%;" name="adress" width="100%" type="text">
									<br/>
									<label for="city">Luogo</label><br/>
									<select style="width:100%;" name="city">
										<option value="Cesena">Cesena</option>
										<option value="Gambettola">Gambettola</option>
										<option value="San Mauro in Valle">San Mauro in Valle</option>
										<option value="Borgo Paglia">Borgo Paglia</option>
										<option value="Ponte Pietra">Ponte Pietra</option>
										<option value="Quartiere Vigne">Quartiere Vigne</option>
										<option value="Quartiere Osservanza">Quartiere Osservanza</option>
										<option value="Campus Universitario">Campus Universitario</option>
										<option value="Martorano">Martorano</option>
										<option value="Cesenatico">Cesenatico</option>
									</select>
									<br/>
									<label for="cap">CAP</label><br/>
									<input pattern=".{5,5}" required title="Lunghezza CAP 5 caratteri" style="width:100%;" name="cap" width="100%" type="text" maxlength="5">
									<br/>
									<label for="telephoneNumber">Telefono</label><br/>
									<input pattern=".{1,10}" required title="Lunghezza minima 1 carattere, massima 10 caratteri" style="width:100%;" name="telephoneNumber" width="100%" type="tel">
									<br/>
								</div>
								<br/>
								<div align="center">
									<input class="submit" style="text-align: center; width:60%;" type="submit" value="Paga"/>
								</div>
								<input type="hidden" value="salva" id="salva" name="salva" />
							</form>
							<?php 
								}
							} ?>
							
						
					<?php
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				
			</div>
		</div>
	</div>
<!-- //welcome -->
<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->

<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
<?php } ?>
