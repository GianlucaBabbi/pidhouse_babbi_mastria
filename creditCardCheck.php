<?php

//mettere a posto le variabili di sessione che vengono passate a pagamento, togliere quel pezzo di codice da quella pagina e metterlo in una'altra pagina che viene chiamata prima di chiamare la pagina di pagamento


 if (isset($_POST['month']) and isset($_POST['year'])) {
	$month= $_POST['month'];
	$year=$_POST['year'];

	$currentMonth= date("m",time());
	$currentYear= date("Y",time());

	if ($month < $currentMonth and $year == $currentYear) {
		header("location: payment.php?result=false");
	} else {
		header("location: addToOrders.php");
	}
} else {
	header("location: payment.php");
}

?>