<?php
session_start();

if(isset($_SESSION['user_id'])) {

//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

$conn =new mysqli($servername, $username, $password, $database);

//mettere controllo anche per rotoli e scegliere cosa fare nel caso il link venga modificato a caso

$tipo;
if (strtolower($_GET['listino']) == "piadine") {
	$tipo = "PIADINE";
} else if (strtolower($_GET['listino']) == "crescioni") {
	$tipo = "CRESCIONI";
} else {
	$tipo = "ROTOLI";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<script type="text/javascript" src="./js/jquery.dNotify.min.js"></script>
<link rel="stylesheet" type="text/css" href="./css/dNotify.css" media="screen" />
<script type="text/javascript" src="./js/jquery.dNotify.js"></script>

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>

<?php 
	if (isset($_GET['result'])) {
		if (strtolower($_GET['result']) == 'true') {
			echo "<script>
					$.dNotify(\"show\", {
   						message: 'Prodotto aggiunto al carrello',
    					sticky: false,
   						duration: 5000,
  						type: 'good',
						iconPosition: 'right'
					});</script>";
		} else {
			echo "<script>$.dNotify('show', {
   					message: 'Prodotto non aggiunto al carrello',
    				sticky: false,
   					duration: 2000,
  					type: 'bad',
					iconPosition: 'right'
					});</script>";
		}
}
?>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="list.php?listino=piadine"><span>Piadine</span></a></li>
										<li><a href="list.php?listino=rotoli"><span>Rotoli</span></a></li>
										<li><a href="list.php?listino=crescioni"><span>Crescioni</span></a></li>
										<li><a href="cart.php"><span><i class="iconColor fa fa-shopping-cart fa-2x" aria-hidden="true"></i></span></a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-hover="Short Codes"><i class="iconColor fa fa-user fa-2x" aria-hidden="true"></i></span> <b class="caret"></b></a>
											<ul class="dropdown-menu agile_short_dropdown">
												<li><a href="orders.php">I miei ordini</a></li>
												<li><a href="personalData.php">I miei dati</a></li>
												<li><a href="index.php">Log out</a></li>
											</ul>
										</li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<!-- welcome -->
	<div class="about">
		<div class="container">
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2><?php 
							
							echo $tipo;
						?>
					</h2>
					<br/>
				</div>
				<?php
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else{
						$query_sql="SELECT * FROM " .$tipo;
						$result = $conn->query($query_sql);
						if($result !== false){
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center">Nome</th><th align="center">Ingredienti</th><th align="center">Prezzo</th><th align="center">Quantità</th><th></th>
						</theader>
						<tbody>
							<?php
								if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {
							?>
							<form method="post" action="addToCartFunction.php">
								<tr>
									<td>
										<input type="hidden" name="type" id="type" value="<?php echo $tipo; ?>">
										<label name="name" id="name"><?php echo $row["nome"]; ?></label>
										<input type="hidden" name="name" id="name" value="<?php echo $row['nome']; ?>">
									</td>
									<td>
										<?php echo $row["ingredienti"]; ?>
									</td>
									<td>
										<?php echo $row["prezzo"]; ?> €
										<input type="hidden" name="price" id="price" value="<?php echo $row['prezzo']; ?>">
									</td>
									<td>
										<select name="quantity" id="quantity">
											<?php for ($i = 1; $i <= 10; ++$i) {?>
												<option><?php echo $i?></option>
											<?php } ?>
										</select>
									</td>
									<td> 
										<input type="image" src="./images/carrello1.png" />
									</td>
								</tr>
							</form>
							<?php 
								}
							} ?>
							
						</tbody>
					</table>
					<?php
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
		</div>
	</div>
<!-- //welcome -->

<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->

<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
<?php }?>

