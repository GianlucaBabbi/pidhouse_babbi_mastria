<?php

//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$passwordDb = "";
$database = "Piadineria";

if(isset($_POST["name"]) and isset($_POST["surname"]) and isset($_POST["mail"]) and isset($_POST["password"]) and isset($_POST["confermaPassword"]) and isset($_POST["indirizzo"]) ){
	
	if ($_POST["password"] == $_POST["confermaPassword"]) {
  		//preparazione query
		$name = $_POST['name'];
		$surname = $_POST['surname'];
		$mail = $_POST['mail'];
		$password =	$_POST['password'];
		$indirizzo = $_POST['indirizzo'];
  		$query_sql="INSERT INTO `utenti_registrati` (`nome`, `cognome`, `email`, `password`, `indirizzo`) VALUES ('$name','$surname','$mail', '$password', '$indirizzo')";
  		//connessione al db
  		$conn=new mysqli($servername, $username, $passwordDb, $database);
  		//Check della connessione
  		if ($conn->connect_errno) {
     		echo "Failed to connect to MySQL";
			
  		} else {
			if ($conn->query($query_sql) === TRUE) {
				echo "<script language='javascript'>
   		 				alert('Utente registrato con successo');
						</script>";
				header("location: index.html");
  			} else {
      			echo "<script language='javascript'>
   		 				alert('Esiste già un utente con questa mail');
						</script>";
 			}
			//Chiusura connessione con db
			$conn->close();
  		}
	} else {
		echo "<script language='javascript'>
   		 alert('Le due password sono differenti');
		</script>";
	}
	
} 

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->
<!-- tabs -->
<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
<!-- //tabs -->
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
</head>
<body>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="index.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li class="active"><a href="index.html"><span><i class="iconColor fa fa-home fa-2x" aria-hidden="true"></i></span></a></li>
										<li><a href="login.php"><span><i class="iconColor fa fa-sign-in fa-2x" aria-hidden="true"></i></span></a></li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<!-- welcome -->
	<div class="about">
		<div class="container">
			<div class="w3-about-grids" style="text-align: center;">
			<!-- in action ci andrà il codice php che verifica se esiste l'utente e la password -->
				<form method="post" action="register.php">
					<div class="label"><label for="name">Nome:</label></div><br/>
					<input style="text-align: center;" type="text" name="name" id="name" required>
					<br/><br/>
					<div class="label"><label for="surname">Cognome:</label></div><br/>
					<input style="text-align: center;" type="text" name="surname" id="surname" required>
					<br/><br/>
					<div class="label"><label for="indirizzo">Indirizzo:</label></div><br/>
					<input style="text-align: center;" type="text" name="indirizzo" id="indirizzo" required>
					<br/><br/>
					<div class="label"><label for="mail">E-mail:</label></div><br/>
					<input style="text-align: center;" type="email" name="mail" id="mail" required>
					<br/><br/>
					<div class="label"><label for="password">Password:</label></div><br/>
					<input style="text-align: center;" type="password" name="password" id="password" required>
					<br/><br/>
					<div class="label"><label for="confermaPassword">Conferma password:</label></div><br/>
					<input style="text-align: center;"  type="password" name="confermaPassword" id="confermaPassword" required>
					<br/><br/>
					<input type="submit" id="submit" value="Registrati">	
				</form>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //welcome -->
<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-envelope" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->
<!-- codice per tornare su veloce -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
	<script src='js/jquery.typer.js'></script>
		<script>
						var win = $(window),
							foo = $('#typer');

						foo.typer(['Piadine', 'Rotoli', 'Crescioni']);
						foo.css('color','black');
						foo.css('font-family', 'Merriweather, serif');
						// unneeded...
						win.resize(function(){
							var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

							foo.css({
								fontSize: fontSize * 5 + 'px'
							});
							
						}).resize();
					</script>
					<!--tabs-->
	<script src="js/easy-responsive-tabs.js"></script>
	
	<!--//tabs-->
</body>
</html>

