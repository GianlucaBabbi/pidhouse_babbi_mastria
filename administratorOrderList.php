<?php
session_start();

if(isset($_SESSION['user_id']) and isset($_SESSION['administrator']) and $_SESSION['administrator'] == 'SI') {

//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

$conn = new mysqli($servername, $username, $password, $database);

$email= $_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->

<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="administratorProductsList.php"><span>Elenco Prodotti</span></a></li>
										<li><a href="addNewFood.php"><span>Aggiungi prodotto</span></a></li>
										<li><a href="index.php"><span><i class="iconColor fa fa-sign-out fa-2x" aria-hidden="true"></i></span></a></li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<!-- welcome -->
	<div class="about">
		<div class="container">
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Ordini in consegna
					</h2>
					<br/>
				</div>
				<?php
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else {
						$query_sql="SELECT * FROM ordini where stato_consegna='IN_CONSEGNA'";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center">N° Ordine</th><th align="center">Email Destinatario</th><th align="center">Prodotti Acquistati</th><th align="center" width="10%">Data</th><th align="center">Prezzo</th><th align="center">Stato Consegna</th><th></th>
						</theader>
						<tbody>
							<?php
								while($row = $result->fetch_assoc()) {
							?>
								<form method="post" action="updateDelivery.php">
									<tr>
										<td>
											<?php echo $row["id"]; ?>
											<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
										</td>
										<td>
											<?php echo $row["email_destinatario"]; ?>
											<input name="emailDestinatario" id="emailDestinatario" type="hidden" value="<?php echo $row['email_destinatario']; ?>"/>
										</td>
										<td>
											<?php echo $row["prodotti_acquistati"]; ?>
										</td>
										<td>
											<?php 
												$data_eng= $row['data']; 
												$data_it= date('d-m-Y', strtotime($data_eng)); 

												echo $data_it;
											?>
										</td>
										<td> 
											<?php echo $row["totale_pagato"]; ?> €
										</td>
										<td> 
											<select name="state" id="state" required>
												<option value="IN_CONSEGNA" selected>IN CONSEGNA</option>
												<option value="DESTINATARIO_NON_TROVATO" >DESTINATARIO NON TROVATO</option>
												<option value="CONSEGNATO">CONSEGNATO</option>
											</select>
										</td>
										<td> 
											<input class="submit" type="submit" value="Salva" />
										</td>
									</tr>
								</form>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							}
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Ordini non consegnati per assenza destinatario
					</h2>
					<br/>
				</div>
				<?php
					$conn = new mysqli($servername, $username, $password, $database);
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else{
						$query_sql="SELECT * FROM ordini where stato_consegna='DESTINATARIO_NON_TROVATO'";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center">N° Ordine</th><th align="center">Prodotti Acquistati</th><th align="center">Data</th><th align="center">Prezzo</th><th align="center">Stato Consegna</th><th></th>
						</theader>
						<tbody>
							<?php
								
								while($row = $result->fetch_assoc()) {
							?>
							<form method="post" action="updateDelivery.php">
								<tr>
									<td>
										<?php echo $row["id"]; ?>
										<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
									</td>
									<td>
										<?php echo $row["prodotti_acquistati"]; ?>
									</td>
									<td>
										<?php echo $row["data"]; ?>
									</td>
									<td> 
										<?php echo $row["totale_pagato"]; ?> €
									</td>
									<td> 
										<select name="state" id="state" required>
											<option value="IN_CONSEGNA">IN CONSEGNA</option>
											<option value="DESTINATARIO_NON_TROVATO" selected>DESTINATARIO NON TROVATO</option>
											<option value="CONSEGNATO">CONSEGNATO</option>
										</select>
									</td>
									<td> 
										<input type="submit" value="Salva" />
									</td>
								</tr>
							</form>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							} else {
								echo "Non ci sono ordini che non sono arrivati al destinatario per sue cause";
							}
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Ordini consegnati
					</h2>
					<br/>
				</div>
				<?php
					$conn = new mysqli($servername, $username, $password, $database);
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else {
						$query_sql="SELECT * FROM ordini where stato_consegna='CONSEGNATO'";
						$result = $conn->query($query_sql);
						if($result !== false){
							if ($result->num_rows > 0) {
				?>
				<div class="table-responsive">
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th align="center">N° Ordine</th><th align="center">Prodotti Acquistati</th><th align="center">Data</th><th align="center">Prezzo</th><th align="center">Stato Consegna</th>
						</theader>
						<tbody>
							<?php
								
								while($row = $result->fetch_assoc()) {
							?>
							
							<tr>
								<td>
									<?php echo $row["id"]; ?>
									<input name="id" id="id" type="hidden" value="<?php echo $row['id']; ?>"/>
								</td>
								<td>
									<?php echo $row["prodotti_acquistati"]; ?>
								</td>
								<td>
									<?php echo $row["data"]; ?>
								</td>
								<td> 
									<?php echo $row["totale_pagato"]; ?> €
								</td>
								<td> 
									<?php echo $row["stato_consegna"]; ?>
								</td>
							</tr>
							<?php 
								}
							 ?>
						</tbody>
					</table>
					<?php
							} else {
								echo "Non sono stati consegnati ordini";
							}
						}
						else {
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
				</div>
			</div>
		</div>
	</div>
<!-- //welcome -->

<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->

<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
<?php } ?>
