<?php 
session_start();

if(isset($_SESSION['user_id'])) {


//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "Piadineria";

//eliminare i prodotti presenti nella tabella non ordinati

				//mettere a posto problema con data per eliminare i record vecchi
				$datadata =date("Y-m-d");
				$query_sql="DELETE FROM `carrello` WHERE data<(NOW() - INTERVAL 1 DAY)";
  				//connessione al db
  				$conn=new mysqli($servername, $username, $password, $database);
  				//Check della connessione
  				if ($conn->connect_errno) {
     				echo "Failed to connect to MySQL";
				} else {
					if ($conn->query($query_sql) === TRUE) {

  					} else {
      					echo "<script language='javascript'>
   		 				alert('C'è stato un problema con il salvataggio, la preghiamo di ritentare successivamente');
						</script>";
 					}
					//Chiusura connessione con db
					$conn->close();
				}

$conn =new mysqli($servername, $username, $password, $database);

$email = $_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pidhouse</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mug house Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/main.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> <!-- serve per le icons -->
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>
</head>
<body>
<!-- banner -->
	<div class="banner">
		<div class="header-bottom">
			<div class="header">
				<div class="container">
					<div class="w3_agile_logo">
						<h1><a href="home.php"><span>PIDhouse</span>la casa della piadina</a></h1>
					</div>
					<div class="header-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Raccoglie i link quando il menù diventa un icona-->
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="link-effect-12">
									<ul class="nav navbar-nav">
										<li><a><span><?php 
													echo "Utente collegato : " .$_SESSION['user_id']?></span></a></li>
										<li><a href="list.php?listino=piadine"><span>Piadine</span></a></li>
										<li><a href="list.php?listino=rotoli"><span>Rotoli</span></a></li>
										<li><a href="list.php?listino=crescioni"><span>Crescioni</span></a></li>
										<li><a href="cart.php"><span><i class="iconColor fa fa-shopping-cart fa-2x" aria-hidden="true"></i></span></a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-hover="Short Codes"><i class="iconColor fa fa-user fa-2x" aria-hidden="true"></i></span> <b class="caret"></b></a>
											<ul class="dropdown-menu agile_short_dropdown">
												<li><a href="orders.php">I miei ordini</a></li>
												<li><a href="personalData.php">I miei dati</a></li>
												<li><a href="index.php">Log out</a></li>
											</ul>
										</li>
									</ul>	
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
<!-- header -->
		</div>
	</div>
<!-- //banner -->
<!-- welcome -->
	<div class="about">
		<div class="container">
			<div class="panel-body">
				<div class="w3ls-heading">
					<h2>Carrello
					</h2>
					<br/>
				</div>
				<?php
					if ($conn->connect_errno) {
				?>
					<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
				<?php
					}
					else{
						$query_sql="SELECT * FROM `carrello` where email_utente=\"" .$email ."\"";
						$result = $conn->query($query_sql);
						if($result !== false){
				?>
				<form method="post" action="deliveryData.php">
				<div class="table-responsive" style="border: 0px !important;">
				
					<table class="table table-striped" cellpadding="2" cellspacing="1">
						<theader>
							<th></th><th></th><th align="center">Nome</th><th align="center">Quantità</th><th align="center">Prezzo</th><th align="center">Totale</th>
						</theader>
						<tbody>
							<?php
								$totale = 0;
								if ($result->num_rows > 0) {
								
								while($row = $result->fetch_assoc()) {

								//creare php per eliminazione di un prodotto da tabella
							?>
							<tr>
								<td><a href="updateCart.php?id=<?php echo $row['id']?>&quantity=<?php echo $row['quantita']?>&type=add"><span><i class="fa fa-plus" style="color:green;" aria-hidden="true"></i></span></a>
								</td>
								<td><a href="updateCart.php?id=<?php echo $row['id']?>&quantity=<?php echo $row['quantita']?>&type='delete'"><span><i class="fa fa-minus" style="color:red;" aria-hidden="true"></i></span></a>
								</td>
								<td> 
									<label name="name" id="name"><?php echo $row["nome_prodotto"]; ?></label>
								</td>
								<td>
									<label name="quantity" id="quantity"><?php 
										$q = $row["quantita"];
										echo $row["quantita"]; ?></label>
								</td>
								<td>
									<label name="price" id="price"><?php echo $row["prezzo"]; ?></label> €
								</td>
								<td>
									<?php 
										$tot = $q * $row["prezzo"];
										$totale = $totale + $tot;
									?>
									<label name="price" id="price"><?php echo $tot ?></label> €
								</td>
							</tr>
							<?php 
								}
							} ?>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="5"> <b>Totale: </b></td>
							<td><b><?php echo $totale?> €</b> <input type="hidden" name="totPrice" value="<?php echo $totale?>"/></td>
							
						</tr>
						</tfoot>
					</table>
					<?php
						}
						else{
					?>
						<p>Errore nell'interrogazione</p>
					<?php
						}
						//Chiusura connessione con db
						$conn->close();
					}
					?>
					<br/><br/>
					</div>
						<input type="submit" class="submit pull-right" value="Procedi con il pagamento"/>
						
					</form>
				
			</div>				
		</div>
	</div>
	
<!-- //welcome -->
<!-- footer -->
	<div class="w3-agile-footer">
		<div class="icons-banner">
				<div class="top-banner-right">
					<ul>
						<li><a href="#"><i class="iconColor fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="iconColor fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>	
			</div>
	</div>
<!-- //footer -->
<!-- start-smooth-scrolling -->
<!-- for bootstrap working    serve per far apparire il menù a tendina-->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- script freccia per tornare in alto quando scorro la pagina -->
<script type="text/javascript">
		$(document).ready(function() {
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
</script>

</body>
</html>
<?php }?>
